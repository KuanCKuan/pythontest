import cv2
import dlib
import numpy as np
import time
from numpy.lib import mixins
import data
from sklearn.metrics.pairwise import cosine_similarity
import sys

landmarks_setting = {
    'left_brow' : [17 , 22],
    'rigth_brow' : [22 , 27],
    'left_eyes' : [36 , 42],
    'rigth_eyes' : [42 , 48],
    'mouth':[47 ,67]
    #'nose':[27 ,36]
    #'jaw':[0,17]
} 

class AU() :
    x = None
    y = None
    start_idx = None
    end_idx = None
    x_scores = None
    y_scores = None

    def set_landmarks(self, list):
        self.start_idx = list[0]
        self.end_idx = list[1]

    def landmarks2list(self , idx):
        l1 = list(landmarks_setting.values())
        result = l1[idx]
        return result
    

class EmotionRecognition():
    haarcascade_file = 'haarcascade_frontalface_default.xml'
    haarcascade_filepath = './assist/haarcascade_frontalface_default.xml'
    predictor_filepath = './assist/shape_predictor_68_face_landmarks.dat'
    predictor = None
    au_list = []
    
    def __init__(self):
        self.predictor = dlib.shape_predictor(self.predictor_filepath)

    def run(self):
        test_path = './happy/h12.jpeg'
        input_img = cv2.imread(test_path)
        input_value = self.get_input_data(input_img)
        self.get_sample_data()
        self.cosine_similarity(input_value , self.au_list)
        Emotion = self.emotion_recognition(self.au_list)
        print(Emotion)
        
        
    def get_input_data(self , img):
        au_temp_listx = []
        au_temp_listy = []
        
        input_landmarks = self._get_face_from_image(img)
        for idx in range(len(landmarks_setting)):
            input_value = AU()
            landmarks = input_value.landmarks2list(idx)
            input_value.set_landmarks(landmarks)
            (list_x,list_y) = self.get_xy(input_value , input_landmarks)
            au_temp_listx.append(list_x)
            au_temp_listy.append(list_y)
        input_value.x = au_temp_listx
        input_value.y = au_temp_listy
        
        return input_value
        
    # New all AU data in au_list.
    def get_sample_data(self):
        s = data.SampleDataTemp()
        au1 = data.AU1()
        au2 = data.AU2()
        au4 = data.AU4()
        au5 = data.AU5()
        au6 = data.AU6()
        au7 = data.AU7()
        au9 = data.AU9()
        au99a = data.AU99a()
        au100a = data.AU100a()
        au101a = data.AU101a()
        au12 = data.AU12()
        au14 = data.AU14()
        au15 = data.AU15()
        au16 = data.AU16()
        au20 = data.AU20()
        au23 = data.AU23()
        au26 = data.AU26()
        au99b = data.AU99b()
        au100b = data.AU100b()
        au101b = data.AU101b()
        
   
        self.au_list.append(au1)
        self.au_list.append(au2)
        self.au_list.append(au4)
        self.au_list.append(au5)
        self.au_list.append(au6)
        self.au_list.append(au7)
        self.au_list.append(au9)
        self.au_list.append(au99a)
        self.au_list.append(au100a)
        self.au_list.append(au101a)
        self.au_list.append(au12)
        self.au_list.append(au14)
        self.au_list.append(au15)
        self.au_list.append(au16)
        self.au_list.append(au20)
        self.au_list.append(au23)
        self.au_list.append(au26)
        self.au_list.append(au99b)
        self.au_list.append(au100b)
        self.au_list.append(au101b)
       
   
        for idx in range(len(self.au_list)):
            self.au_list[idx].get_data()
            s.get_xy_list(self.au_list[idx])

        return None
        
    # Get input image x,y.
    def get_xy(self, au, landmarks):
        list_x=[]
        list_y=[]
        start_idx = au.start_idx
        end_idx = au.end_idx
        marks = landmarks[start_idx:end_idx]
        for idx, point in enumerate(marks):
            pos = (point[0, 0], point[0, 1])
            list_x.append(pos[0])
            list_y.append(pos[1])
        return list_x , list_y

    # Get Landmarks from input face images.
    def _get_face_from_image(self, img):
        detector = dlib.get_frontal_face_detector()
        img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        faces = detector(img_gray, 0)
        landmarks = None
        if len(faces) != 0:
            for i in range(len(faces)):
                landmarks = np.matrix([[p.x, p.y] for p in self.predictor(img, faces[i]).parts()])
        return landmarks

    #def emotion_recognition(self, input_au, sample_au):
        # Compute score and recognize emotion
        scores_list = []
        Emotion_list = ['Happy','Sad','Suprise','Fear','Angry','Disgust','Contempt']
        AU_face_count = 10 # AU1 ~ AU100a
        AU_mouth_count = 10 # AU12 ~ AU100b
        four_parts_of_face = 4 # 2 brows and 2 eyes
        input_mouth_index = 4 # mouth is index of (left_brow, rigth_brow, left_eyes, rigth_eyes, mouth)
        AU_mouth_start_number = 10 # start at AU100a  
        mouth_list_index = 0 # mouth only one list
        
        face_x_scores = []
        face_y_scores = []
        mouth_x_scores =[]
        mouth_y_scores =[]
        for j in range(AU_face_count):
            face_x_list=[]
            face_y_list=[]
            for i in range(four_parts_of_face):
                vec_input = np.array(input_au.x[i])
                vec_sample = np.array(sample_au[j].x[i])
                # Transpose vector to calculate Cosine similarity
                vec_sample_transpose = vec_sample.T
                cos_sim_x = np.dot(vec_input, vec_sample_transpose) / (np.linalg.norm(vec_input) * np.linalg.norm(vec_sample))
                face_x_list.append( (cos_sim_x).tolist() )

                vec_input = np.array(input_au.y[i])
                vec_sample = np.array(sample_au[j].y[i])
                vec_sample_transpose = vec_sample.T
                cos_sim_y = np.dot(vec_input, vec_sample_transpose) / (np.linalg.norm(vec_input) * np.linalg.norm(vec_sample))
                face_y_list.append( (cos_sim_y).tolist() )

            face_x_scores.append( sum(face_x_list)/ len(face_x_list) )
            face_y_scores.append( sum(face_y_list)/ len(face_y_list) )
    
            sample_au[j].x_scores = face_x_scores[0]
            sample_au[j].y_scores = face_y_scores[0]

        for k in range(AU_mouth_count):
            vec_inputy = np.array(input_au.x[input_mouth_index])
            vec_sampley = np.array(sample_au[AU_mouth_start_number + k].x[mouth_list_index])
            vec_sampley_transpose = vec_sampley.T
            mouth_x = np.dot(vec_inputy, vec_sampley_transpose) / (np.linalg.norm(vec_inputy) * np.linalg.norm(vec_sampley))
            mouth_x_list = (mouth_x).tolist()
            mouth_x_scores.append( mouth_x_list )

            vec_inputy = np.array(input_au.y[input_mouth_index])
            vec_sampley = np.array(sample_au[AU_mouth_start_number + k].y[mouth_list_index])
            vec_sampley_transpose = vec_sampley.T
            mouth_y = np.dot(vec_inputy, vec_sampley_transpose) / (np.linalg.norm(vec_inputy) * np.linalg.norm(vec_sampley))
            mouth_y_list = (mouth_y).tolist()
            mouth_y_scores.append( mouth_y_list )

            sample_au[AU_mouth_start_number+k].x_scores = mouth_x_list
            sample_au[AU_mouth_start_number+k].y_scores = mouth_y_list

        scores_list.append(self.compute_happy_score(sample_au))
        scores_list.append(self.compute_sad_score(sample_au))
        scores_list.append(self.compute_surprise_score(sample_au))


        return Emotion_list[ scores_list.index(max(scores_list)) ] 

    # Use cosine similarity to compute scores
    def cosine_similarity(self, input_au, sample_au):
        four_parts_of_face = 4 # 2 brows and 2 eyes
        AU_face_count = 10 # AU1 ~ AU101a
        AU_mouth_count = 10 ## AU12 ~ AU100b
        input_mouth_index = 4 # mouth is index of (left_brow, rigth_brow, left_eyes, rigth_eyes, mouth)
        AU_mouth_start_number = 10 # start at AU101a  
        mouth_list_index = 0 # mouth only one list

        for j in range(AU_face_count):
            temp_x_list=[]
            temp_y_list=[]
            for i in range(four_parts_of_face):
                input_face_x = np.array(input_au.x[i])
                sample_face_x = np.array(sample_au[j].x[i])
                cos_sim_facex = cosine_similarity([input_face_x], [sample_face_x])
                temp_x_list.append(cos_sim_facex)
           
                input_face_y = np.array(input_au.y[i])
                sample_face_y = np.array(sample_au[j].y[i])
                cos_sim_facey = cosine_similarity([input_face_y], [sample_face_y])
                temp_y_list.append(cos_sim_facey)
            sample_au[j].x_scores = np.nansum(temp_x_list)
            sample_au[j].y_scores = np.nansum(temp_y_list)

        for k in range(AU_mouth_count):
            mouth_x_list =[]
            mouth_y_list =[]
            input_mouth_x = np.array(input_au.x[input_mouth_index]) # np.array(input_au.x[4])
            sample_mouth_x = np.array(sample_au[AU_mouth_start_number + k].x[mouth_list_index])# np.array(sample_au[10+k].x[0]
            cos_sim_mouthx = cosine_similarity([input_mouth_x], [sample_mouth_x])
            mouth_x_list.append(cos_sim_mouthx)

            input_mouth_y = np.array(input_au.y[input_mouth_index])
            sample_mouth_y = np.array(sample_au[AU_mouth_start_number + k].y[mouth_list_index])
            cos_sim_mouthy = cosine_similarity([input_mouth_y], [sample_mouth_y])
            mouth_y_list.append(cos_sim_mouthy)

            sample_au[AU_mouth_start_number+k].x_scores = np.nansum(mouth_x_list)
            sample_au[AU_mouth_start_number+k].y_scores = np.nansum(mouth_y_list)

    # Compare scores to recognize emotion
    def emotion_recognition(self, sample_au):
        scores_list = []
        Emotion_list = ['Happy','Sad','Suprise','Fear','Angry','Disgust','Contempt']

        scores_list.append(self.compute_happy_score(sample_au))
        scores_list.append(self.compute_sad_score(sample_au))
        scores_list.append(self.compute_surprise_score(sample_au))


        return Emotion_list[ scores_list.index(max(scores_list)) ] 

    def compute_happy_score(self, sample_au):
        au6 = ( sample_au[4].x_scores/4 + sample_au[4].y_scores/4 )/2
        au12 = (sample_au[10].x_scores + sample_au[10].y_scores)/2
        au99a = ( sample_au[7].x_scores/4 + sample_au[7].y_scores/4 )/2
        au99b = (sample_au[17].x_scores + sample_au[17].y_scores)/2

        Happy_score = (au6 + au12)/2
        Happy_score2 = (au99a + au99b)/2

        return max(Happy_score ,Happy_score2)
        
    def compute_sad_score(self, sample_au):
        au1 = ( sample_au[0].x_scores/4 + sample_au[0].y_scores/4 )/2
        au4 = ( sample_au[2].x_scores/4 + sample_au[2].y_scores/4 )/2
        au15 = (sample_au[12].x_scores + sample_au[12].y_scores)/2
        au100a = ( sample_au[8].x_scores/4 + sample_au[8].y_scores/4 )/2
        au100b = (sample_au[18].x_scores + sample_au[18].y_scores)/2

        Sad_score  = (au1 + au15)/2
        Sad_score2 = (au4 + au15)/2
        Sad_score3 = (au100a + au100b)/2

        return max(Sad_score ,Sad_score2 ,Sad_score3)

    def compute_surprise_score(self, sample_au):
        au1 = ( sample_au[0].x_scores/4 + sample_au[0].y_scores/4 )/2
        au2 = ( sample_au[1].x_scores/4 + sample_au[1].y_scores/4 )/2
        au5 = ( sample_au[3].x_scores/4 + sample_au[3].y_scores/4 )/2
        au26 = (sample_au[16].x_scores + sample_au[16].y_scores)/2
        au101a = ( sample_au[9].x_scores/4 + sample_au[9].y_scores/4 )/2
        au101b = (sample_au[19].x_scores + sample_au[19].y_scores)/2
        
        Surprise_score  = (au1 + au26)/2
        Surprise_score2 = (au2 + au26)/2
        Surprise_score3 = (au5 + au26)/2
        Surprise_score4 = (au101a + au101b)/2

        return max(Surprise_score ,Surprise_score2, Surprise_score3, Surprise_score4)

    def compute_fear_score(self, face_x_scores, face_y_scores, mouth_x_scores, mouth_y_scores):
        Fear_score  = (face_x_scores[0] + face_y_scores[0] + mouth_x_scores[4] + mouth_y_scores[4] )/4
        Fear_score2 = (face_x_scores[1] + face_y_scores[1] + mouth_x_scores[4] + mouth_y_scores[4] )/4
        Fear_score3 = (face_x_scores[2] + face_y_scores[2] + mouth_x_scores[4] + mouth_y_scores[4] )/4
        Fear_score4 = (face_x_scores[3] + face_y_scores[3] + mouth_x_scores[4] + mouth_y_scores[4] )/4
        Fear_score5 = (face_x_scores[5] + face_y_scores[5] + mouth_x_scores[4] + mouth_y_scores[4] )/4
        Fear_score6 = (face_x_scores[0] + face_y_scores[0] + mouth_x_scores[6] + mouth_y_scores[6] )/4
        Fear_score7 = (face_x_scores[1] + face_y_scores[1] + mouth_x_scores[6] + mouth_y_scores[6] )/4
        Fear_score8 = (face_x_scores[2] + face_y_scores[2] + mouth_x_scores[6] + mouth_y_scores[6] )/4
        Fear_score9 = (face_x_scores[3] + face_y_scores[3] + mouth_x_scores[6] + mouth_y_scores[6] )/4
        Fear_score10 = (face_x_scores[5] + face_y_scores[5] + mouth_x_scores[6] + mouth_y_scores[6] )/4
        return max(Fear_score ,Fear_score2 ,Fear_score3 ,Fear_score4 ,Fear_score5 ,Fear_score6 ,Fear_score7 ,Fear_score8 ,Fear_score9 ,Fear_score10)

    def compute_angry_score(self, face_x_scores, face_y_scores, mouth_x_scores, mouth_y_scores):
        Angry_score  = (face_x_scores[2] + face_y_scores[2] + mouth_x_scores[5] + mouth_y_scores[5] )/4
        Angry_score2 = (face_x_scores[3] + face_y_scores[3] + mouth_x_scores[5] + mouth_y_scores[5] )/4
        Angry_score3 = (face_x_scores[5] + face_y_scores[5] + mouth_x_scores[5] + mouth_y_scores[5] )/4
        return max(Angry_score ,Angry_score2 ,Angry_score3)
    
    def compute_disgust_score(self, face_x_scores, face_y_scores, mouth_x_scores, mouth_y_scores):
        Disgust_score  = (face_x_scores[6] + face_y_scores[6] + mouth_x_scores[2] + mouth_y_scores[2] )/4
        Disgust_score2 = (face_x_scores[6] + face_y_scores[6] + mouth_x_scores[3] + mouth_y_scores[3] )/4
        return max(Disgust_score ,Disgust_score2)

    def compute_contempt_score(self, mouth_x_scores, mouth_y_scores):
        Contempt_score  = (mouth_x_scores[0] + mouth_y_scores[0] )/2
        Contempt_score2 = (mouth_x_scores[1] + mouth_y_scores[1] )/2
        return max(Contempt_score ,Contempt_score2)
 

if __name__ == "__main__":
    #filepath = ''
    #if sys.argv[1]:
    e = EmotionRecognition()
    e.run()
  

